
var $jBNLPOSitivity = jQuery.noConflict();

$jBNLPOSitivity(document).ready(function() {
    var methodObj = $jBNLPOSitivity('#method');
    var acquirerObj = $jBNLPOSitivity('#acquirer-container');
    if(methodObj.length && acquirerObj.length){
        methodObj.on('change', function(){
            renewAcquirer();
        });
        renewAcquirer();
        function renewAcquirer(){
            if(methodObj.val() === 'computop')
                acquirerObj.show();
            else
                acquirerObj.hide();
        }
    }

    var checkoutTypeObj = $jBNLPOSitivity('#checkouttype');
    var paymentInstrumentsContainerObj = $jBNLPOSitivity('#payment_instruments_container');
    if(checkoutTypeObj.length && paymentInstrumentsContainerObj.length){
        checkoutTypeObj.on('change', function(){
            renewPaymentInstruments();
        });
        renewPaymentInstruments();
        function renewPaymentInstruments(){
            if(parseInt(checkoutTypeObj.val()) === 3 || parseInt(checkoutTypeObj.val()) === 2)
                paymentInstrumentsContainerObj.show();
            else
                paymentInstrumentsContainerObj.hide();
        }

        var tidfindomesticObj = $jBNLPOSitivity('#tidfindomestic_container');
        var paymentInstrumentsObj = $jBNLPOSitivity('.payment_instruments');
        if(tidfindomesticObj.length && paymentInstrumentsObj.length){
            paymentInstrumentsObj.on('change', function(){
                renewTidFindomestic();
            });
            renewTidFindomestic();
            function renewTidFindomestic(){
                if(paymentInstrumentsObj.val() === null || paymentInstrumentsObj.val().indexOf("findomestic") === -1)
                    tidfindomesticObj.hide();
                else
                    tidfindomesticObj.show();
            }
        }
    }

    var tidObj = $jBNLPOSitivity('#tid');
    var tidFindomesticObj = $jBNLPOSitivity('#tidfindomestic');
    var ksigObj = $jBNLPOSitivity('#ksig');
    var hMacPasswordObj = $jBNLPOSitivity('#hMacPassword');
    var testDataButtonObj = $jBNLPOSitivity('#testdatabutton');
    if(tidObj.length && ksigObj.length && testDataButtonObj.length){
        testDataButtonObj.on('click', function (event) {
            event.preventDefault();
            tidObj.val($jBNLPOSitivity(this).data('test-tid'));
            ksigObj.val($jBNLPOSitivity(this).data('test-ksig'));
            if(hMacPasswordObj.length)
                hMacPasswordObj.val($jBNLPOSitivity(this).data('test-hmacpassword'));
            if(tidFindomesticObj.length)
              tidFindomesticObj.val($jBNLPOSitivity(this).data('test-tid-findomestic'));
        });
    }

    var tokenenabledObj = $jBNLPOSitivity('[name="token_enabled"]');
    var oneclicktokenenabledObj = $jBNLPOSitivity('#one_click_token_enabled');

    if(tokenenabledObj.length && oneclicktokenenabledObj.length){

        tokenenabledObj.on('change', function(){
            tokenenableRenew($jBNLPOSitivity(this).val());
        });

        tokenenableRenew(tokenenabledObj.filter(":checked").val());

        function tokenenableRenew(value){
            console.log(value);
            if(value !== '1')
                oneclicktokenenabledObj.hide();
            else
                oneclicktokenenabledObj.show();
        }

    }

    var multicurrencyObj = $jBNLPOSitivity('#multicurrency');
    var currencytableObj = $jBNLPOSitivity('#currencytable');

    if(multicurrencyObj.length && currencytableObj.length){

        multicurrencyObj.find('input').on('change', function(){
            currencytableRenew();
        });

        currencytableRenew();

        function currencytableRenew(){

            if(multicurrencyObj.find('input').filter(":checked").val() !== '1'){
                currencytableObj.find('tbody tr[data-show="multicurrency"]').hide();
                currencytableObj.find('tbody tr[data-show="singlecurrency"]').show();
            }
            else
                currencytableObj.find('tbody tr[data-show="multicurrency"]').show();
        }
    }
});