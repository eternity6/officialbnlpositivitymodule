
var $jBNLPOSitivity = jQuery.noConflict();

$jBNLPOSitivity(document).ready(function() {
    var methodObj = $jBNLPOSitivity('#payment_bnlpositivity_paymentservice_method');

    if(!methodObj.length)
        return;

    var checkoutTypeObj = $jBNLPOSitivity('#payment_bnlpositivity_paymentservice_checkouttype');
    var testDataButtonObj = $jBNLPOSitivity('#row_payment_bnlpositivity_paymentservice_testdatabutton');
    var testmodeObj = $jBNLPOSitivity('#row_payment_bnlpositivity_paymentservice_testmode');

    var tidIgfsObj = $jBNLPOSitivity('#payment_bnlpositivity_paymentservice_tid_igfs');
    var tidFindomesticObj = $jBNLPOSitivity('#payment_bnlpositivity_paymentservice_tidfindomestic');
    var ksigIgfsObj = $jBNLPOSitivity('#payment_bnlpositivity_paymentservice_ksig_igfs');
    var tidComputopObj = $jBNLPOSitivity('#payment_bnlpositivity_paymentservice_tid_computop');
    var ksigComputopObj = $jBNLPOSitivity('#payment_bnlpositivity_paymentservice_ksig_computop');

    var hMacPasswordObj = $jBNLPOSitivity('#payment_bnlpositivity_paymentservice_hMacPassword');
    var paymentInstrumentsIgfsObj = $jBNLPOSitivity('#payment_bnlpositivity_paymentservice_payment_instruments_igfs');
    var tidFindomesticRowObj = $jBNLPOSitivity('#row_payment_bnlpositivity_paymentservice_tidfindomestic');
    var multicurrencyObj = $jBNLPOSitivity('#row_payment_bnlpositivity_paymentservice_multicurrency');
    var currencytableObj = $jBNLPOSitivity('#row_payment_bnlpositivity_paymentservice_multicurrency_table');
    var languagetableObj = $jBNLPOSitivity('#row_payment_bnlpositivity_paymentservice_language_table');
    var tokenEnabledObj = $jBNLPOSitivity('#row_payment_bnlpositivity_paymentservice_token_enabled');
    var oneClickTockenEnabledObj = $jBNLPOSitivity('#row_payment_bnlpositivity_paymentservice_one_click_token_enabled');
    var sendNotifyObj = $jBNLPOSitivity('#row_payment_bnlpositivity_paymentservice_send_notify');
    var logEnabledObj = $jBNLPOSitivity('#row_payment_bnlpositivity_paymentservice_log');
    var logdateRowObj = $jBNLPOSitivity('#row_payment_bnlpositivity_paymentservice_logdate');
    var logdateFromObj = logdateRowObj.find('[name="_from"]');
    var logdateToObj = logdateRowObj.find('[name="_to"]');
    var logdateButtonObj = logdateRowObj.find('button');

    if(testmodeObj.length && testDataButtonObj.length){
        methodObj.on('change', function(){
            tidIgfsObj.val("");
            tidFindomesticObj.val("");
            ksigIgfsObj.val("");
            tidComputopObj.val("");
            ksigComputopObj.val("");
            hMacPasswordObj.val("");
        });

        testmodeObj.find('input').on('change', function(){
            renewTestDataButton();
            setTestData();
        });

        testDataButtonObj.find('button').on('click', function(event) {
            event.preventDefault();
            setTestData();
        });

        renewTestDataButton();
        function renewTestDataButton(){
            if(testmodeObj.find('input').filter(":checked").val() === '1')
                testDataButtonObj.show();
            else
                testDataButtonObj.hide();
        }

        function setTestData(){
            if(testmodeObj.find('input').filter(":checked").val() === '1'){
                tidIgfsObj.val(testDataButtonObj.find('button').data('test-tid-igfs'));
                tidFindomesticObj.val(testDataButtonObj.find('button').data('test-tidfindomestic-igfs'));
                ksigIgfsObj.val(testDataButtonObj.find('button').data('test-ksig-igfs'));
                tidComputopObj.val(testDataButtonObj.find('button').data('test-tid-computop'));
                ksigComputopObj.val(testDataButtonObj.find('button').data('test-ksig-computop'));
                hMacPasswordObj.val(testDataButtonObj.find('button').data('test-hmacpassword-computop'));
            }else{
                tidIgfsObj.val('');
                tidFindomesticObj.val('');
                ksigIgfsObj.val('');
                tidComputopObj.val('');
                ksigComputopObj.val('');
                hMacPasswordObj.val('');
            }
        }
    }

    if(paymentInstrumentsIgfsObj.length && tidFindomesticRowObj.length){

        methodObj.on('change', function(){
            paymentInstrumentsTidFindomestic(paymentInstrumentsIgfsObj.val());
        });

        if(tidFindomesticRowObj.length){
            paymentInstrumentsIgfsObj.on('change', function(){
                paymentInstrumentsTidFindomestic($jBNLPOSitivity(this).val());
            });
            paymentInstrumentsTidFindomestic(paymentInstrumentsIgfsObj.val());
        }

        function paymentInstrumentsTidFindomestic(value){
            if(value === null || value.indexOf("findomestic") === -1 || methodObj.val() !== 'igfs')
                tidFindomesticRowObj.hide();
            else
                tidFindomesticRowObj.show();
        }
    }

    if(tokenEnabledObj.length && methodObj.length && oneClickTockenEnabledObj.length){

        methodObj.on('change', function(){
            tockenFieldsRenew($jBNLPOSitivity(this).val(), tokenEnabledObj.val());
        });

        tokenEnabledObj.find('input').on('change', function(){
            tockenFieldsRenew(methodObj.val(), $jBNLPOSitivity(this).val());
        });

        tockenFieldsRenew(methodObj.val(), tokenEnabledObj.find('input').filter(":checked").val());

        function tockenFieldsRenew(methodValue, tokenEnabledValue){
            if(methodValue !== 'igfs' || tokenEnabledValue !== '1')
                oneClickTockenEnabledObj.hide();
            else
                oneClickTockenEnabledObj.show();
        }
    }

    if(sendNotifyObj.length && methodObj.length){
        methodObj.on('change', function(){
            notifyFieldsRenew($jBNLPOSitivity(this).val());
        });

        notifyFieldsRenew(methodObj.val());

        function notifyFieldsRenew(methodValue){
            if(methodValue !== 'igfs')
                sendNotifyObj.hide();
            else
                sendNotifyObj.show();
        }
    }

    if(logEnabledObj.length && logdateRowObj.length){
        logEnabledObj.find('input').on('change', function(){
            logFieldsRenew($jBNLPOSitivity(this).val());
        });

        logFieldsRenew(logEnabledObj.find('input').filter(":checked").val());

        function logFieldsRenew(value){
            if(value !== '1')
                logdateRowObj.hide();
            else
                logdateRowObj.show();
        }

        logdateFromObj.on('change', function() {
            renewLogdateButtonRoute();
        });

        logdateToObj.on('change', function() {
            renewLogdateButtonRoute();
        });

        logdateButtonObj.on('click', function() {
            var win = window.open($jBNLPOSitivity(this).data('route') + '?from=' + logdateFromObj.val() + '&to=' + logdateToObj.val(),'_blank');
            win.focus();
        });

        function renewLogdateButtonRoute(){
            if(logdateFromObj.val() === '' || logdateToObj.val() === ''){
                logdateButtonObj.addClass('disabled').prop('disabled', true);
            } else{
                logdateButtonObj.removeClass('disabled').prop('disabled', false);
            }
        }
    }

    if(multicurrencyObj.length && currencytableObj.length){

        methodObj.on('change', function(){
            currencytableRenew();
        });

        multicurrencyObj.find('input').on('change', function(){
            currencytableRenew();
        });

        currencytableRenew();

        function currencytableRenew(){

            console.log('currencytableRenew');

            currencytableObj.find('tbody tr').not('[data-method="'+methodObj.val()+'"]').hide();

            if(methodObj.val() === 'igfs'){
                multicurrencyObj.show();
            }else{
                multicurrencyObj.find('input[value="0"]').prop('checked', true);
                multicurrencyObj.hide();
            }

            if(multicurrencyObj.find('input').filter(":checked").val() !== '1'){
                currencytableObj.find('tbody tr[data-show="multicurrency"]').hide();
                currencytableObj.find('tbody tr[data-show="singlecurrency"][data-method="'+methodObj.val()+'"]').show();
            }
            else
                currencytableObj.find('tbody tr[data-show="multicurrency"][data-method="'+methodObj.val()+'"]').show();
        }
    }

    if(languagetableObj.length){

        methodObj.on('change', function(){
            languagetableRenew();
        });

        languagetableRenew();

        function languagetableRenew(){
            languagetableObj.find('tbody tr').not('[data-method="'+methodObj.val()+'"]').hide();
            languagetableObj.find('tbody tr[data-method="'+methodObj.val()+'"]').show();
        }
    }
});