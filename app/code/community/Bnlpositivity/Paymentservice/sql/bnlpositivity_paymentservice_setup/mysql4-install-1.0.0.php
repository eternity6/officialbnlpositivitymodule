<?php
$installer = $this;

/* @var $installer Mage_Customer_Model_Entity_Setup */
$installer->startSetup();

$installer->addAttribute('customer', 'payinstrtoken', array(
  'type' => 'text',
  'visible' => false,
  'required' => false,
  'user_defined' => false
));

$columnAttribute = array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable'  => true,
    'length'    => 1000,
    'after'     => null,
    'comment'   => 'Bnlpositivity reserved field'
);

$installer->getConnection()->addColumn($installer->getTable('sales/order'),'bnlpositivity_payment_id', $columnAttribute);
$installer->getConnection()->addColumn($installer->getTable('sales/order'),'bnlpositivity_shop_id', $columnAttribute);
$installer->getConnection()->addColumn($installer->getTable('sales/order'),'bnlpositivity_payment_status', $columnAttribute);
$installer->getConnection()->addColumn($installer->getTable('sales/order'),'bnlpositivity_payment_error', $columnAttribute);
$installer->getConnection()->addColumn($installer->getTable('sales/order'),'bnlpositivity_xid', $columnAttribute);
$installer->getConnection()->addColumn($installer->getTable('sales/order'),'bnlpositivity_pcnr', $columnAttribute);
$installer->getConnection()->addColumn($installer->getTable('sales/order'),'bnlpositivity_payment_confirmtranid', $columnAttribute);

$installer->endSetup();