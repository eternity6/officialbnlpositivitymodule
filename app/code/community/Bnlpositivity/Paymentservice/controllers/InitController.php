<?php

class Bnlpositivity_Paymentservice_InitController extends Mage_Core_Controller_Front_Action
{

    public function redirectAction()
    {
        $helper = Mage::helper('bnlpositivity_paymentservice');

        try{

            $session = Mage::getSingleton('checkout/session');
            $session->setBnlpositivityQuoteId($session->getQuoteId());
            $quote = Mage::getModel('sales/quote')->load($session->getQuoteId());
            $order = Mage::getModel('sales/order')->loadByIncrementId($quote->getReservedOrderId());

            $payment = $order->getPayment();
            $method = $payment->getMethodInstance();

            $response = $helper->init($order, $method->getInfoInstance()->getAdditionalInformation());
            if($response)
                return Mage::app()->getResponse()->setRedirect($response) ->sendResponse();

        }catch (Exception $exception){
            $helper->log(sprintf('Exception: %s', $exception->getMessage()));
            $session->addNotice($exception->getMessage());
        }


        return $this->_redirect('checkout/cart', array('_secure' => true));
    }

    public function successAction()
    {
        if(!isset($_SERVER['QUERY_STRING']) || empty($_SERVER['QUERY_STRING']))
            return $this->getResponse()->setHttpResponseCode(404);

        $session = Mage::getSingleton('checkout/session');
        $helper = Mage::helper('bnlpositivity_paymentservice');

        try {

            $order = Mage::getModel('sales/order')->loadByIncrementId($this->getRequest()->getParam('orderid'));
            //$order_session = $session->getLastRealOrder();

            if(!$order->getId() || !$order->getPayment()->getMethod() === Bnlpositivity_Paymentservice_Model_Payment::CODE){
                return $this->getResponse()->setHttpResponseCode(404);
            }

            $response = false;
            if($order->getState() === Mage_Sales_Model_Order::STATE_NEW)
                $response = $helper->verify($order);

            if($order->getBnlpositivityPaymentStatus() === Bnlpositivity_Paymentservice_Model_Payment::STATE_CANCELED){
                $order->cancel()->save();
                $session->addNotice($helper->__('Il tuo ordine è stato cancellato'));
                if($response && is_array($response) && isset($response['errorMessage']) && $response['errorMessage'] && !empty($response['errorMessage']))
                    $session->addNotice(urldecode($response['errorMessage']));
                return $this->_redirect('checkout/cart', array('_secure' => true));
            } else{
                $session = Mage::getSingleton('checkout/session');
                $session->setQuoteId($session->getPaypalStandardQuoteId(true));
                Mage::getSingleton('checkout/session')->getQuote()->setIsActive(false)->save();
                return $this->_redirect('checkout/onepage/success', array('_secure' => true));
            }

        }catch (Exception $exception) {
            $helper->log(sprintf('Verify Exception: (%s) %s', $exception->getCode(), $exception->getMessage()));
            return $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function notifyAction()
    {
        if(!isset($_SERVER['QUERY_STRING']) || empty($_SERVER['QUERY_STRING']))
            return $this->getResponse()->setHttpResponseCode(404);

        $session = Mage::getSingleton('checkout/session');
        $helper = Mage::helper('bnlpositivity_paymentservice');

        try {

            $order = Mage::getModel('sales/order')->loadByIncrementId($this->getRequest()->getParam('orderid'));
            //$order_session = $session->getLastRealOrder();

            if(!$order->getId() || !$order->getPayment()->getMethod() === Bnlpositivity_Paymentservice_Model_Payment::CODE){
                return $this->getResponse()->setHttpResponseCode(404);
            }

            $response = false;
            if($order->getState() === Mage_Sales_Model_Order::STATE_NEW)
                $response = $helper->verify($order);

            if($order->getBnlpositivityPaymentStatus() === Bnlpositivity_Paymentservice_Model_Payment::STATE_CANCELED){
                $order->cancel()->save();
                $session->addNotice($helper->__('Il tuo ordine è stato cancellato'));
                if($response && is_array($response) && isset($response['errorMessage']) && $response['errorMessage'] && !empty($response['errorMessage']))
                    $session->addNotice(urldecode($response['errorMessage']));
                return $this->_redirect('checkout/cart', array('_secure' => true));
            } else{
                $session = Mage::getSingleton('checkout/session');
                $session->setQuoteId($session->getPaypalStandardQuoteId(true));
                Mage::getSingleton('checkout/session')->getQuote()->setIsActive(false)->save();
                return $this->_redirect('checkout/onepage/success', array('_secure' => true));
            }

        }catch (Exception $exception) {
            $helper->log(sprintf('Verify Exception: (%s) %s', $exception->getCode(), $exception->getMessage()));
            return $this->getResponse()->setHttpResponseCode(500);
        }
    }

    public function errorAction()
    {
        $helper = Mage::helper('bnlpositivity_paymentservice');
        $session = Mage::getSingleton('checkout/session');

        $order = Mage::getModel('sales/order')->loadByIncrementId($this->getRequest()->getParam('orderid'));
        //$order_session = $session->getLastRealOrder();

        if(!$order->getId() || !$order->getPayment()->getMethod() === Bnlpositivity_Paymentservice_Model_Payment::CODE || $order->getState() !== Mage_Sales_Model_Order::STATE_NEW){
            return $this->getResponse()->setHttpResponseCode(404);
        }

        $response = $helper->verify($order);
        $order->cancel()->save();
        $session->addNotice($helper->__('Il tuo ordine è stato cancellato'));

        if($response && is_array($response) && isset($response['errorMessage']) && $response['errorMessage'] && !empty($response['errorMessage']))
            $session->addNotice(urldecode($response['errorMessage']));

        return $this->_redirect('checkout/cart', array('_secure' => true));
    }
}