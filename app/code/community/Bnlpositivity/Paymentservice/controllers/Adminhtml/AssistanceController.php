<?php

class Bnlpositivity_Paymentservice_Adminhtml_AssistanceController extends Mage_Adminhtml_Controller_Action
{
  public function indexAction()
  {
    if ( $this->getRequest()->getPost() ) {

      $helper = Mage::helper('bnlpositivity_paymentservice');

      if($this->_validateFormKey()){
        $cc         = $this->getRequest()->getParam('cc', null);
        $message    = $this->getRequest()->getParam('message', null);
        if(!empty(trim($message))){

          $payment_instruments = explode(',', $helper->getConfigByCurrentMethod('payment_instruments'));
          $payment_instruments = array_filter($payment_instruments);

          $message .= sprintf('<br><br><h3>Configuration</h3><br>');
          $message .= sprintf('<strong>Method</strong>: %s<br>', $helper->getConfig('method'));
          $message .= sprintf('<strong>Checkout type</strong>: %s<br>', $helper->getConfig('checkouttype'));
          $message .= sprintf('<strong>Acquirer</strong>: %s<br>', $helper->getConfig('acquirer'));
          $message .= sprintf('<strong>Payment instruments</strong>: %s<br>', implode(', ', $payment_instruments));
          $message .= sprintf('<strong>trtype</strong>: %s<br>', $helper->getConfigByCurrentMethod('trtype'));
          $message .= sprintf('<strong>Tid</strong>: %s<br>', $helper->getConfigByCurrentMethod('tid'));
          $message .= sprintf('<strong>Tid Findomestic</strong>: %s<br>', $helper->getConfig('tidfindomestic'));
          $message .= sprintf('<strong>Ksig</strong>: %s<br>', $helper->getConfigByCurrentMethod('ksig'));
          $message .= sprintf('<strong>hMacPassword</strong>: %s<br>', $helper->getConfigByCurrentMethod('hMacPassword'));
          $message .= sprintf('<strong>Token enabled</strong>: %s<br>', $helper->getConfig('token_enabled'));
          $message .= sprintf('<strong>One click buy enabled</strong>: %s<br>', $helper->getConfig('one_click_token_enabled'));
          $message .= sprintf('<strong>Sandbox</strong>: %s<br>', $helper->getConfig('testmode'));
          $message .= sprintf('<strong>Currency</strong>: %s<br>', Mage::app()->getStore()->getCurrentCurrencyCode());
          $message .= sprintf('<strong>Language</strong>: %s<br>', Mage::app()->getLocale()->getLocaleCode());

          $mail = Mage::getModel('core/email');
          $mail->setToEmail('ecommercesupport@bnlpositivity.it');
          $mail->setBody($message);
          $mail->setSubject(__('%s [Assistance request]', Mage::app()->getStore()->getName()));
          $mail->setFromEmail(Mage::getStoreConfig('trans_email/ident_general/email'));
          $mail->setFromName(Mage::app()->getStore()->getName());
          $mail->setType('html');

          if(filter_var($cc, FILTER_VALIDATE_EMAIL) !== false)
            $mail->setToCc($cc);

          try {
            $mail->send();

            $this->_getSession()->addSuccess($helper->__('Your request has been sent'));
            return $this->_redirect('adminhtml/system_config/edit/', array('section' => 'payment'));
          }
          catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            return $this->_redirect('*/*/*');
          }

        }else{
          $this->_getSession()->addError($helper->__('Invalid message.'));
        }
      }else{
        $this->_getSession()->addError($helper->__('Invalid Form Key. Please refresh the page.'));
      }
    }

    $this->loadLayout();
    $this->getLayout()->getBlock("head")->setTitle(Mage::helper('bnlpositivity_paymentservice')->__("BNLPOSitivity Assistance Request"));
    $this->renderLayout();

    return $this;
  }
}