<?php

class Bnlpositivity_Paymentservice_Adminhtml_LogController extends Mage_Adminhtml_Controller_Action
{
  public function indexAction()
  {

    $fileCollection = '';

    $dateFrom = DateTime::createFromFormat('d/m/Y H:i:s', sprintf('%s 00:00:00', $this->getRequest()->get('from')));
    $dateTo = DateTime::createFromFormat('d/m/Y H:i:s', sprintf('%s 23:59:59', $this->getRequest()->get('to')));

    if($dateFrom > $dateTo)
      $dateFrom = DateTime::createFromFormat('d/m/Y H:i:s', sprintf('%s 00:00:00', $this->getRequest()->get('to')));

    $iterateDateFrom = DateTime::createFromFormat('d/m/Y H:i:s', sprintf('%s 00:00:00', $this->getRequest()->get('from')));
    $iterateDateTo = DateTime::createFromFormat('d/m/Y H:i:s', sprintf('%s 23:59:59', $this->getRequest()->get('to')));

    if($iterateDateFrom > $iterateDateTo)
      $iterateDateFrom = DateTime::createFromFormat('d/m/Y H:i:s', sprintf('%s 00:00:00', $this->getRequest()->get('to')));

    $logBasePath = Mage::getBaseDir('log') ;


    if(is_dir($logBasePath)){
      while ($iterateDateFrom <= $iterateDateTo){
        $filePath = sprintf('%s/%s-%s.log', $logBasePath, Bnlpositivity_Paymentservice_Model_Payment::CODE, $iterateDateFrom->format('Y_m_d'));

        if(is_file($filePath)){
          if(!empty($fileCollection))
            $fileCollection .= "\n\n";
          $fileCollection .= sprintf("***** Log date: %s", $iterateDateFrom->format('Y-m-d'));
          $fileCollection .= "\n\n";
          $fileCollection .= @file_get_contents($filePath);
          $fileCollection .= "\n\n\n\n";
        }
        $iterateDateFrom->modify('+1 day');
      }
    }

    if(empty($fileCollection))
      $fileCollection .= sprintf('No logs found between %s - %s', date('d-m-Y', $dateFrom), date('d-m-Y', $dateTo));

    header("Expires: 0");
    header("Cache-Control: no-cache, no-store, must-revalidate");
    header('Cache-Control: pre-check=0, post-check=0, max-age=0', false);
    header("Pragma: no-cache");
    header("Content-type: text/html");
    header("Content-Disposition:attachment; filename=" . Bnlpositivity_Paymentservice_Model_Payment::CODE . "-log.txt");
    header("Content-Length: " . strlen($fileCollection));

    echo $fileCollection;
    exit();
  }
}