<?php

class Bnlpositivity_Paymentservice_Adminhtml_WizardController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $step = intval($this->getRequest()->getParam('step', null));
        if(!$step || $step === null)
            return $this->_redirect('*/*/*/', array('step' => 1));

        $helper = Mage::helper('bnlpositivity_paymentservice');


        if ( $this->getRequest()->getPost() ) {

            if($step > 6){
                $this->_getSession()->addSuccess($helper->__('Configuration procedure completed'));
                return $this->_redirect('adminhtml/system_config/edit/', array('section' => 'payment'));
            }

            if ($this->_validateFormKey()) {

                $post = $this->getRequest()->getPost();

                if(isset($post['form_key']))
                    unset($post['form_key']);

                foreach($post as $key => $item)
                    $helper->setConfig($key, is_array($item) ? implode(',', $item) : $item);

                if($step === 1):
                  $helper->setConfig('title', 'BNLPOSitivity Payment Gateway');
                  $helper->setConfig('description', 'Allow your customers to pay comfortably by credit cards');
                endif;

                $step++;
                return $this->_redirect('*/*/*/', array('step' => $step));
            }else{
                $this->_getSession()->addError($helper->__('Invalid Form Key. Please refresh the page.'));
            }
        }

        $this->loadLayout();
        $this->getLayout()->getBlock("head")->setTitle(Mage::helper('bnlpositivity_paymentservice')->__("BNLPOSitivity Setup Wizard"));
        $headerBlock = $this->getLayout()->createBlock(
            'Mage_Core_Block_Template',
            'bnlpositivity-wizard-header',
            array('template' => 'bnlpositivity/wizard/header.phtml')
        );

        $contentBlock = $this->getLayout()->createBlock(
            'Mage_Core_Block_Template',
            'bnlpositivity-wizard-first-step',
            array('template' => sprintf('bnlpositivity/wizard/step_%s.phtml', $step))
        );

        $headerBlock->setData('step', $step);
        $contentBlock->setData('step', $step);

        $this->getLayout()->getBlock('content')->getChild('bnlpositivity-wizard-container')->append($headerBlock);
        $this->getLayout()->getBlock('content')->getChild('bnlpositivity-wizard-container')->append($contentBlock);

        $this->renderLayout();
    }
}