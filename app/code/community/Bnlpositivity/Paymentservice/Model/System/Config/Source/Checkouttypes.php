<?php

class Bnlpositivity_Paymentservice_Model_System_Config_Source_Checkouttypes{
  public function toOptionArray()
  {
    $helper = Mage::helper('bnlpositivity_paymentservice');

    $checkouttypes = array();

    foreach($helper->getCheckoutTypes() as $key => $item)
      $checkouttypes[] = array(
        'value' => $key,
        'label' => $item,
      );

    return $checkouttypes;
  }
}