<?php

class Bnlpositivity_Paymentservice_Model_System_Config_Source_Sellinglocation{
  public function toOptionArray()
  {
    $helper = Mage::helper('bnlpositivity_paymentservice');

    $sellinglocations = array();

    foreach($helper->getSellingLocations() as $key => $item)
      $sellinglocations[] = array(
        'value' => $key,
        'label' => $item,
      );

    return $sellinglocations;
    
  }
}