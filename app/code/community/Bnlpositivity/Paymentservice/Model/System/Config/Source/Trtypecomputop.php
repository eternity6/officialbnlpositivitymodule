<?php


class Bnlpositivity_Paymentservice_Model_System_Config_Source_Trtypecomputop{
  public function toOptionArray()
  {
    $helper = Mage::helper('bnlpositivity_paymentservice');

    $paymentinstruments = array();

    foreach($helper->getTrtypeComputop() as $key => $item)
      $paymentinstruments[] = array(
        'value' => $key,
        'label' => $item,
      );

    return $paymentinstruments;
  }
}