<?php

class Bnlpositivity_Paymentservice_Model_System_Config_Source_Acquirer{
  public function toOptionArray()
  {
    $helper = Mage::helper('bnlpositivity_paymentservice');

    $acquirers = array();

    foreach($helper->getAcquirers() as $key => $item)
      $acquirers[] = array(
        'value' => $key,
        'label' => $item,
      );

    return $acquirers;
  }
}