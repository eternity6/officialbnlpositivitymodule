<?php


class Bnlpositivity_Paymentservice_Model_System_Config_Source_Trtypeigfs{
  public function toOptionArray()
  {
    $helper = Mage::helper('bnlpositivity_paymentservice');

    $paymentinstruments = array();

    foreach($helper->getTrtypeIgfs() as $key => $item)
      $paymentinstruments[] = array(
        'value' => $key,
        'label' => $item,
      );

    return $paymentinstruments;
  }
}