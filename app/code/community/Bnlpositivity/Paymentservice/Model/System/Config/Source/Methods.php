<?php

class Bnlpositivity_Paymentservice_Model_System_Config_Source_Methods{
  public function toOptionArray()
  {
    return array(
      array(
        'value' => 'igfs',
        'label' => 'BNL POSitivity 1',
      ),
      array(
        'value' => 'computop',
        'label' => 'BNL POSitivity 2',
      ),
    );
  }
}