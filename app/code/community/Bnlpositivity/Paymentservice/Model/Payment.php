<?php

class Bnlpositivity_Paymentservice_Model_Payment extends Mage_Payment_Model_Method_Abstract
{

  const CODE                              = 'bnlpositivity_paymentservice';
  const STATE_AUTHORIZED                  = 'authorized';
  const STATE_COMPLETE                    = 'complete';
  const STATE_NEW                         = 'new';
  const STATE_PROCESSING                  = 'processing';
  const STATE_CANCELED                    = 'canceled';

  protected $_code                        = self::CODE;
  protected $_formBlockType               = 'bnlpositivity_paymentservice/checkout_form';
  protected $_infoBlockType               = 'bnlpositivity_paymentservice/checkout_info';
  protected $_isInitializeNeeded          = true;
  protected $_canUseInternal              = false;
  protected $_canUseForMultishipping      = false;

  protected $_isGateway                   = true;
  protected $_canOrder                    = true;
  protected $_canAuthorize                = true;
  protected $_canCapture                  = true;
  protected $_canCapturePartial           = true;
  protected $_canCaptureOnce              = true;
  protected $_canRefund                   = true;
  protected $_canRefundInvoicePartial     = true;
  protected $_canVoid                     = false;
  protected $_canUseCheckout              = true;
  protected $_canFetchTransactionInfo     = false;
  protected $_canReviewPayment            = false;
  protected $_canCreateBillingAgreement   = false;
  protected $_canManageRecurringProfiles  = false;

  /**
   * Assign data to info model instance
   *
   * @param   mixed $data
   * @return  Mage_Payment_Model_Info
   */
  public function assignData($data)
  {
    $result = parent::assignData($data);

    try{
      $this->getInfoInstance()->setAdditionalInformation('bnlpositivity_paymentservice_payment_instrument', $data->getBnlpositivityPaymentservicePaymentInstrument());
      $this->getInfoInstance()->setAdditionalInformation('bnlpositivity_paymentservice_scheme', $data->getBnlpositivityPaymentserviceScheme());
      $this->getInfoInstance()->setAdditionalInformation('bnlpositivity_paymentservice_bic', $data->getBnlpositivityPaymentserviceBic());
      $this->getInfoInstance()->setAdditionalInformation('bnlpositivity_paymentservice_iban', $data->getBnlpositivityPaymentserviceIban());
    }catch (\Exception $exception){

    }

    return $result;
  }

  public function getTitle(){
    return Mage::helper('bnlpositivity_paymentservice')->__('BNLPOSitivity Payment Gateway');// $helper->getConfig('description');
  }

  /**
   * Validate payment method information object
   *
   * @param   Mage_Payment_Model_Info $info
   * @throws  Mage_Core_Exception
   * @return  Bnlpositivity_Paymentservice_Model_Payment
   */
  public function validate()
  {
    parent::validate();
    $info = $this->getInfoInstance();

    $helper = Mage::helper('bnlpositivity_paymentservice');
    if (intval($helper->getConfig('checkouttype')) === 3 && !$info->getAdditionalInformation('bnlpositivity_paymentservice_payment_instrument'))
      Mage::throwException(Mage::helper('bnlpositivity_paymentservice')->__("Please select a payment instrument"));

    return $this;
  }

  /**
   * Return Order place redirect url
   *
   * @return string
   */
  public function getOrderPlaceRedirectUrl()
  {
    return Mage::getUrl('bnlpositivity_paymentservice/init/redirect', array('_secure' => true));
  }

  /**
   * Check whether payment method can be used
   * @param Mage_Sales_Model_Quote
   * @return bool
   */
  public function isAvailable($quote = null)
  {
    return Mage::getStoreConfig('payment/bnlpositivity_paymentservice/active');
  }

  /**
   * Capture payment
   *
   * @param Varien_Object $payment
   * @param float $amount
   * @return Bnlpositivity_Paymentservice_Model_Payment
   * @throws Mage_Core_Exception
   */
  public function capture(Varien_Object $payment, $amount)
  {
    $helper = Mage::helper('bnlpositivity_paymentservice');

    $order          = $payment->getOrder();
    $transaction    = $payment->getAuthorizationTransaction();

    if (!$this->canCapture() || !$transaction || !$helper->confirm($order, $transaction->getTxnId(), $amount)) {
      Mage::throwException($helper->__('Capture action is not available.'));
    }

    return $this;
  }

  /**
   * Refund payment
   *
   * @param Varien_Object $payment
   * @param float $amount
   * @return Bnlpositivity_Paymentservice_Model_Payment
   * @throws Mage_Core_Exception
   */
  public function refund(Varien_Object $payment, $amount)
  {
    $helper = Mage::helper('bnlpositivity_paymentservice');

    $order              = $payment->getOrder();
    $transaction        = $payment->getAuthorizationTransaction();
    $confirm_tranid     = $order->getBnlpositivityPaymentConfirmtranid();
    if($confirm_tranid && !empty($confirm_tranid))
      $tran_id = $confirm_tranid;
    elseif($transaction)
      $tran_id = $transaction->getTxnId();
    else
      $tran_id = false;

    if (!$this->canRefund() || !$tran_id || !$helper->refund($payment->getOrder(), $tran_id, $amount)) {
      Mage::throwException(Mage::helper('bnlpositivity_paymentservice')->__('Refund action is not available.'));
    }

    return $this;
  }
}