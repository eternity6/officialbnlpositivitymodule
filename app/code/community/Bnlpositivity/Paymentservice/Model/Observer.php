<?php
class Bnlpositivity_Paymentservice_Model_Observer
{

    protected static $registered = false;

    public function addAutoloader(Varien_Event_Observer $observer)
    {
        // this should not be necessary.  Just being done as a check
        if (self::$registered) {
            return;
        }
        spl_autoload_register(array($this, 'autoload'), false, true);
        self::$registered = true;
    }

    public function autoload($class)
    {
        // project-specific namespace prefix
        $prefix = 'Payment\\';

        // base directory for the namespace prefix
        $base_dir = Mage::getBaseDir('lib') . '/Bnlpositivity/';

        if(strpos($class, 'PayGateway') !== false):
            require_once $base_dir . 'PayGateway.php';
            return;
        endif;

        if(strpos($class, $prefix) !== false):
            $file = $base_dir . str_replace('\\', '/', $class) . '.php';
            require_once $file;
            return;
        endif;
    }

    public function cron(){

        $orders = Mage::getModel('sales/order')
            ->getCollection()->addFieldToFilter('status', array('eq' => 'pending'));


        if($orders && count($orders)):

            $helper = Mage::helper('bnlpositivity_paymentservice');

            foreach($orders as $order):
                if($order->getPayment()->getMethod() === Bnlpositivity_Paymentservice_Model_Payment::CODE):
                    $helper->verify($order);
                endif;
            endforeach;
        endif;

        return true;
    }
}
