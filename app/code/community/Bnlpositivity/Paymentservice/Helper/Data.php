<?php

define( 'BNLPOSITIVITY_LANG', 'it_IT' );

class Bnlpositivity_Paymentservice_Helper_Data extends Mage_Core_Helper_Abstract
{

  private $method;
  private $testmode;
  private $log;
  private $payGateway;

  public function __construct() {
    $this->method = $this->getConfig('method');
    $this->testmode = $this->getConfig('testmode');
    $this->log = $this->getConfig('log');
    $this->payGateway = Bnlpositivity\Payment\PayGateway::getIstance($this->method && $this->method !== '' ? $this->method : 'igfs', $this->testmode);
  }

  public function getConfig($field){
    return Mage::getStoreConfig(sprintf('payment/bnlpositivity_paymentservice/%s', $field));
  }

  public function getConfigByCurrentMethod($field){
    return Mage::getStoreConfig(sprintf('payment/bnlpositivity_paymentservice/%s_%s', $field, $this->method));
  }

  public function setConfig($field, $value){
    Mage::getModel('core/config')->saveConfig(sprintf('payment/bnlpositivity_paymentservice/%s', $field), $value);
  }

  public static function bnlpositivity_placeholders($content, $order){
    $content = str_replace('{site_title}', Mage::app()->getStore()->getName(), $content);
    $content = str_replace('{order_number}', $order->getIncrementId(), $content);
    $content = str_replace('{customer_name}', $order->getBillingAddress()->getName(), $content);
    $content = str_replace('{customer_email}', $order->getBillingAddress()->getEmail(), $content);
    $content = str_replace('{order_date}', $order->getCreatedAt(), $content);
    return $content;
  }

  public function getPayGateway(){
    return $this->payGateway;
  }

  public function log($message){

    $date = new \DateTime('now');
    Mage::log($message, null, sprintf('%s-%s.log', Bnlpositivity_Paymentservice_Model_Payment::CODE, $date->format('Y_m_d')));
  }

  /*** HELPER FUNCTIONS ***/

  public function getMethods(){
    return array(
      ''          => __('Select a method', 'bnlpositivity'),
      'igfs'      => 'BNL POSitivity 1',
      'computop'  => 'BNL POSitivity 2',
    );
  }

  public function getMethod(){
    return $this->method;
  }

  public function getCheckoutTypes(){
    return $this->payGateway->getCheckoutTypes();
  }

  public function getPaymentInstruments(){
    return $this->payGateway->getPaymentInstruments();
  }

  public function getTrtypes(){
    return $this->payGateway->getTransactionTypes();
  }

  public function getTestTid(){
    return $this->payGateway->getTestTerminalId();
  }

  public function getTestTidFindomestic(){
    return $this->payGateway->getTestFindomesticTerminalId();
  }

  public function getTestKsig(){
    return $this->payGateway->getTestHashMessage();
  }

  public function getTesthMacPassword(){
    return $this->payGateway->getTesthMacPassword();
  }

  public function getAcquirers(){
    return $this->payGateway->getAcquirer();
  }

  public function getCurrenciesAllowed(){
    return $this->payGateway->getCurrenciesAllowed();
  }

  public function getPaymentInstrumentsIgfs(){
    return Bnlpositivity\Payment\PayGateway::getIstance('igfs', $this->testmode)->getPaymentInstruments();
  }

  public function getPaymentInstrumentsComputop(){
    return Bnlpositivity\Payment\PayGateway::getIstance('computop', $this->testmode)->getPaymentInstruments();
  }

  public function getTrtypeIgfs(){
    return Bnlpositivity\Payment\PayGateway::getIstance('igfs', $this->testmode)->getTransactionTypes();
  }

  public function getTrtypeComputop(){
    return Bnlpositivity\Payment\PayGateway::getIstance('computop', $this->testmode)->getTransactionTypes();
  }

  public function getCurrenciesAllowedIgfs(){
    return Bnlpositivity\Payment\PayGateway::getIstance('igfs', $this->testmode)->getCurrenciesAllowed();
  }

  public function getCurrenciesAllowedComputop(){
    return Bnlpositivity\Payment\PayGateway::getIstance('computop', $this->testmode)->getCurrenciesAllowed();
  }

  public function getLanguagesAllowedIgfs(){
    return Bnlpositivity\Payment\PayGateway::getIstance('igfs', $this->testmode)->getLanguagesAllowed();
  }

  public function getLanguagesAllowedComputop(){
    return Bnlpositivity\Payment\PayGateway::getIstance('computop', $this->testmode)->getLanguagesAllowed();
  }

  public function getSellingLocations(){
    return Bnlpositivity\Payment\PayGateway::getIstance('computop', $this->testmode)->getSellingLocations();
  }

  public function getTestTidIgfs(){
    return Bnlpositivity\Payment\PayGateway::getIstance('igfs', $this->testmode)->getTestTerminalId();
  }

  public function getTestTidFindomesticIgfs(){
    return Bnlpositivity\Payment\PayGateway::getIstance('igfs', $this->testmode)->getTestFindomesticTerminalId();
  }

  public function getTestKsigIgfs(){
    return Bnlpositivity\Payment\PayGateway::getIstance('igfs', $this->testmode)->getTestHashMessage();
  }

  public function getTestTidComputop(){
    return Bnlpositivity\Payment\PayGateway::getIstance('computop', $this->testmode)->getTestTerminalId();
  }

  public function getTestKsigComputop(){
    return Bnlpositivity\Payment\PayGateway::getIstance('computop', $this->testmode)->getTestHashMessage();
  }

  public function getTesthMacPasswordComputop(){
    return Bnlpositivity\Payment\PayGateway::getIstance('computop', $this->testmode)->getTesthMacPassword();
  }

  /**
   * Restore last active quote based on checkout session
   *
   * @return bool True if quote restored successfully, false otherwise
   */
  public function restoreQuote()
  {
    $session = Mage::getSingleton('checkout/session');
    $order = $session->getLastRealOrder();
    if ($order->getId()) {
      $quote = Mage::getModel('sales/quote')->load($order->getQuoteId());
      if ($quote->getId()) {
        $quote->setIsActive(1)
          ->setReservedOrderId(null)
          ->save();
        $session
          ->replaceQuote($quote)
          ->unsLastRealOrderId();
        return true;
      }
    }
    return false;
  }

  /**
   * Check whether order can be processed
   * @param Mage_Sales_Model_Order $order
   * @return bool
   */
  public function canProcessOrder($order){
    return $order->getPayment()->getMethod() === 'bnlpositivity_paymentservice' && intval($this->getConfig('active')) === 1;
  }

  /**
   * Init payment
   * @param Mage_Sales_Model_Order $order
   * @return bool|string
   */
  public function init($order, $args){

    $session = Mage::getSingleton('checkout/session');

    if(!$order->getId()){
      $session->addNotice($this->__('Order not found.'));
      return false;
    }

    $shop_id = time();

    $date = new \DateTime('now');
    $date->modify('+1 day');

    $payInstrToken = '';
    if(filter_var($this->getConfig('token_enabled'), FILTER_VALIDATE_BOOLEAN) === true && Mage::getSingleton('customer/session')->isLoggedIn()):
      $payInstrToken = Mage::getSingleton('customer/session')->getCustomer()->getPayinstrtoken();
      if(!$payInstrToken || empty($payInstrToken)):
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $payInstrToken = hash_hmac('md5', sprintf('%s-%s-bnlpositivity', time(), $customer->getId()), 'secret');
        $customer->setPayinstrtoken($payInstrToken);
        $customer->save();
      endif;
    endif;

    if(filter_var($this->getConfig('token_enabled'), FILTER_VALIDATE_BOOLEAN) !== true)
      $payInstrToken = '';

    $code = Bnlpositivity_Paymentservice_Model_Payment::CODE;
    $payment_instrument = isset($args[sprintf('%s_payment_instrument', $code)]) ? $args[sprintf('%s_payment_instrument', $code)] : '';
    $scheme = isset($args[sprintf('%s_payment_scheme', $code)]) ? $args[sprintf('%s_payment_scheme', $code)] : '';
    $bic = isset($args[sprintf('%s_payment_bic', $code)]) ? $args[sprintf('%s_payment_bic', $code)] : '';
    $iban = isset($args[sprintf('%s_payment_iban', $code)]) ? $args[sprintf('%s_payment_iban', $code)] : '';

    $isMobile = Zend_Http_UserAgent_Mobile::match(Mage::helper('core/http')->getHttpUserAgent(), $_SERVER) ? 'mobile' : 'desktop';
    $street = is_array($order->getShippingAddress()->getStreet()) && count($order->getShippingAddress()->getStreet()) ? $order->getShippingAddress()->getStreet()[0] : '';

    $params = array(
      'orderReference'        => $shop_id,
      'paymentReference'      => $shop_id,
      'terminalId'            => $this->getConfigByCurrentMethod('tid'),
      'terminalIdFindomestic' => $this->getConfig('tidfindomestic'),
      'hashMessage'           => $this->getConfigByCurrentMethod('ksig'),
      'hMacPassword'          => $this->getConfig('hMacPassword'),
      'baseURL'               => '',
      'notifyUrl'             => substr(Mage::getUrl('bnlpositivity_paymentservice/init/notify', array('_secure' => true, 'orderid' => $order->getIncrementId())), 0, 512),
      'callbackUrl'           => substr(Mage::getUrl('bnlpositivity_paymentservice/init/success', array('_secure' => true, 'orderid' => $order->getIncrementId())), 0, 512),
      'errorUrl'              => substr(Mage::getUrl('bnlpositivity_paymentservice/init/error', array('_secure' => true, 'orderid' => $order->getIncrementId())), 0, 512),
      'orderKey'              => $order->getIncrementId(),
      'amount'                => $order->getGrandTotal(),
      'transactionType'       => $this->getConfigByCurrentMethod('trtype'),
      'description'           => substr('Casuale', 0, 100),
      'language'              => Mage::app()->getLocale()->getLocaleCode(),
      'paymentMethod'         => $payment_instrument,
      'currency'              => Mage::app()->getStore()->getCurrentCurrencyCode(),
      'checkoutMode'          => $this->getConfig('checkouttype'),
      'addInfo1'              => substr($this->bnlpositivity_placeholders($this->getConfig('addInfo1'), $order), 0, 256),
      'addInfo2'              => substr($this->bnlpositivity_placeholders($this->getConfig('addInfo2'), $order), 0, 256),
      'addInfo3'              => substr($this->bnlpositivity_placeholders($this->getConfig('addInfo3'), $order), 0, 256),
      'addInfo4'              => substr($this->bnlpositivity_placeholders($this->getConfig('addInfo4'), $order), 0, 256),
      'addInfo5'              => substr($this->bnlpositivity_placeholders($this->getConfig('addInfo5'), $order), 0, 256),
      'payInstrToken'         => $payInstrToken,
      'shopUserRef'           => $this->getConfig('notify') === true ? substr(sprintf('%s', $order->get_billing_email()), 0, 256) : '',
      'regenPayInstrToken'    => '',
      'acquirer'              => $this->getConfig('acquirer'),

      'template'              => $this->getConfig('template'),
      'logoUrl'               => $this->getConfig('logourl'),
      'shippingDetails'       => sprintf('%s, %s, %s', $street, $order->getShippingAddress()->getPostcode(), $order->getShippingAddress()->getCountry()),
      'invoiceDetails'        => sprintf('%s, %s, %s', $order->getBillingAddress()->getFirstname(), $order->getBillingAddress()->getLastname(), $street),

      //COMPUTOP Extra values

      'sellingPoint'          => Mage::app()->getStore()->getName(),
      'device'                => $isMobile,
      'email'                 => Mage::getStoreConfig('trans_email/ident_general/email'),
      'phone'                 => $this->getConfig('phone'),
      'mobileNo'              => $this->getConfig('mobile'),
      'accOwner'              => $this->getConfig('accOwner'),
      'addrCountryCode'       => $this->getConfig('addrCountryCode'),
      'expirationTime'        => $date->format('Y-m-dTh:i:s'),

      'scheme'                => $scheme,
      'bic'                   => $bic,
      'iban'                  => $iban,
      'testmode'              => $this->getConfig('testmode'),

    );

    $this->log("\n");
    $this->log(sprintf('***** (INIT) Process payment for order: %s', $order->getIncrementId()));

    foreach($params as $key => $param)
      $this->log(sprintf('%s: %s', $key, $param));

    $response = $this->getPayGateway()->init($params);

    $this->log("------RESPONSE------");
    foreach($response as $key => $param)
      $this->log(sprintf('%s: %s', $key, $param));

    if($response['error'] === false):
      $order->setBnlpositivityPaymentId($response['paymentID']);
      $order->setBnlpositivityShopId($response['orderReference']);
      $order->save();
      return $response['redirectURL'];
    else:
      $session->addNotice($this->__('Error payment process'));
      return false;
    endif;
  }

  /**
   * Verify payment
   * @param Mage_Sales_Model_Order $order
   * @return array
   */
  public function verify($order){

    $payment_id =  $order->getBnlpositivityPaymentId();
    $shop_id    =  $order->getBnlpositivityShopId();

    $params = array(
      'orderReference'        => $shop_id,
      'paymentID'             => $payment_id,
      'terminalId'            => $this->getConfigByCurrentMethod('tid'),
      'terminalIdFindomestic' => $this->getConfig('tidfindomestic'),
      'hashMessage'           => $this->getConfigByCurrentMethod('ksig'),
      'hMacPassword'          => $this->getConfig('hMacPassword'),
      'language'              => Mage::app()->getLocale()->getLocaleCode(),
      'testmode'              => $this->getConfig('testmode'),
      'UrlParams'             => isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '',
    );

    $this->log("\n");
    $this->log(sprintf('***** (VERIFY) Verify order: %s', $order->getIncrementId()));

    foreach($params as $key => $param)
      $this->log(sprintf('%s: %s', $key, $param));

    $response = $this->getPayGateway()->verify($params);

    $this->log("------RESPONSE------");
    foreach($response as $key => $param)
      $this->log(sprintf('%s: %s', $key, $param));

    if($response['error'] === false){

      $order->setBnlpositivityPaymentError(null);

      if(isset($response['paymentID']) && $response['paymentID'] !== $payment_id)
        $order->setBnlpositivityPaymentId($response['paymentID']);

      if(isset($response['XID']))
        $order->setBnlpositivityXid($response['XID']);

      if(isset($response['PCNr']))
        $order->setBnlpositivityPcnr($response['PCNr']);

      $payment = $order->getPayment();
      $payment->setTransactionId($response['tranID'])
        ->setCurrencyCode($order->getBaseCurrencyCode())
        ->setParentTransactionId(null)
        ->setShouldCloseParentTransaction(true)
        ->setIsTransactionClosed(false);

      switch ($this->getConfigByCurrentMethod('trtype')){
        case 'AUTH':
        case 'MANUAL':
          $order->setBnlpositivityPaymentStatus(Bnlpositivity_Paymentservice_Model_Payment::STATE_AUTHORIZED);
          $order->addStatusHistoryComment($this->__('Payment authorized. Use specific function to capture funds.'));
          $payment->registerAuthorizationNotification($order->getGrandTotal());
          break;
        case 'PURCHASE':
        case 'AUTO':
          $order->setBnlpositivityPaymentStatus(Bnlpositivity_Paymentservice_Model_Payment::STATE_COMPLETE);
          $order->addStatusHistoryComment($this->__('Payment completed.'));
          $payment->registerCaptureNotification($order->getGrandTotal());
          break;
      }

      $order->save();

      try {
        $invoice = $payment->getCreatedInvoice();
        if ($invoice && !$order->getEmailSent()) {
          $order->queueNewOrderEmail()->addStatusHistoryComment(
            Mage::helper('paypal')->__('Notified customer about invoice #%s.', $invoice->getIncrementId())
          )
            ->setIsCustomerNotified(true)
            ->save();
        }
      } catch (Exception $e) {
        Mage::throwException($this->__('Can not send new order email.'));
      }

    }elseif($response['returnCode'] !== 'IGFS_814'){ // transazione in corso
      $order->cancel();
      $order->setBnlpositivityPaymentStatus(Bnlpositivity_Paymentservice_Model_Payment::STATE_CANCELED);
      $order->setBnlpositivityPaymentError($response['returnCode']);
      $order->addStatusHistoryComment(sprintf('Payment verification has failed, (%s) %s', $response['returnCode'], $response['message']));
    }

    if(isset($response['error']) && $response['error'] !== false && isset($response['message']))
      $order->setData('errorMessage', ucfirst(strtolower(urldecode($response['message']))));

    $order->save();

    return array(
        'order' => $order,
        'errorMessage' => isset($response['error']) && $response['error'] !== false && isset($response['message']) && !empty($response['message']) ? ucfirst(strtolower(urldecode($response['message']))) : false
    );
  }

  /**
   * Confirm payment
   * @param Mage_Sales_Model_Order $order
   * @param string $transactionId
   * @param float $amount
   * @return bool
   */
  public function confirm($order, $transactionId, $amount){

    $payment_id =  $order->getBnlpositivityPaymentId();
    $shop_id    =  $order->getBnlpositivityShopId();

    $params = array(
      'orderReference'        => $shop_id,
      'paymentID'             => $payment_id,
      'terminalId'            => $this->getConfigByCurrentMethod('tid'),
      'terminalIdFindomestic' => $this->getConfig('tidfindomestic'),
      'hashMessage'           => $this->getConfigByCurrentMethod('ksig'),
      'hMacPassword'          => $this->getConfig('hMacPassword'),
      'language'              => Mage::app()->getLocale()->getLocaleCode(),
      'testmode'              => $this->getConfig('testmode'),
      'acquirer'              => $this->getConfig('acquirer'),
      'currency'              => Mage::app()->getStore()->getCurrentCurrencyCode(),
      'paymentReference'      => $transactionId,
      'amount'                => $amount,
    );

    try{
      $this->log("\n");
      $this->log(sprintf('***** (CONFIRM) Confirm order: %s', $order->get_id()));

      foreach($params as $key => $param)
        $this->log(sprintf('%s: %s', $key, $param));

      $response = $this->getPayGateway()->confirm($params);

      $this->log("------RESPONSE------");
      foreach($response as $key => $param)
        $this->log(sprintf('%s: %s', $key, $param));

      if($response['error'] === false):
        $order->setBnlpositivityPaymentStatus(Bnlpositivity_Paymentservice_Model_Payment::STATE_COMPLETE);
        $order->setBnlpositivityPaymentConfirmtranid($response['tranID']);
        return true;
      else:
        return false;
      endif;
    }catch (Exception $exception){
      $this->log(sprintf('Exception: %s', $exception->getMessage()));
      return false;
    }
  }

  /**
   * Refund payment
   * @param Mage_Sales_Model_Order $order
   * @param string $transactionId
   * @param float $amount
   * @return bool
   */
  public function refund($order, $transactionId, $amount){

    $payment_id         = $order->getBnlpositivityPaymentId();
    $shop_id            = $order->getBnlpositivityShopId();
    $xid                = $order->getBnlpositivityXid();
    $payment_status     = $order->getBnlpositivityPaymentStatus();

    $params = array(
      'orderReference'        => $shop_id,
      'paymentID'             => $payment_id,
      'terminalId'            => $this->getConfigByCurrentMethod('tid'),
      'terminalIdFindomestic' => $this->getConfig('tidfindomestic'),
      'hashMessage'           => $this->getConfigByCurrentMethod('ksig'),
      'hMacPassword'          => $this->getConfig('hMacPassword'),
      'language'              => Mage::app()->getLocale()->getLocaleCode(),
      'testmode'              => $this->getConfig('testmode'),
      'acquirer'              => $this->getConfig('acquirer'),
      'currency'              => Mage::app()->getStore()->getCurrentCurrencyCode(),
      'paymentReference'      => $transactionId,
      'amount'                => $amount,
      'xId'                   => '',
    );

    try{
      $this->log("\n");
      $this->log(sprintf('***** (REFUND) Refund order: %s', $order->get_id()));

      foreach($params as $key => $param)
        $this->log(sprintf('%s: %s', $key, $param));

      if($payment_status === Bnlpositivity_Paymentservice_Model_Payment::STATE_COMPLETE):
        $response = $this->getPayGateway()->refund($params);
      elseif($payment_status === Bnlpositivity_Paymentservice_Model_Payment::STATE_AUTHORIZED):
        $response = $this->getPayGateway()->cancel($params);
      else:
        return false;
      endif;

      $this->log("------RESPONSE------");
      foreach($response as $key => $param)
        $this->log(sprintf('%s: %s', $key, $param));

      if($response['error'] === false)
        return true;
      else
        return false;

    }catch (Exception $exception){
      $this->log(sprintf('Exception: %s', $exception->getMessage()));
    }

    return false;
  }
}