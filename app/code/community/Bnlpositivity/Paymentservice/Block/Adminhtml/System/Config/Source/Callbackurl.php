<?php

class Bnlpositivity_Paymentservice_Block_Adminhtml_System_Config_Source_Callbackurl extends Mage_Adminhtml_Block_System_Config_Form_Field{

  protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
  {
    return sprintf('%sbnlpositivity_paymentservice/init/success/', Mage::getBaseUrl());
  }
}