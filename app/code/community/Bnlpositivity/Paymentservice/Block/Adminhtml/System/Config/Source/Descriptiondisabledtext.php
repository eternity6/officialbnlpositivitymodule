<?php

class Bnlpositivity_Paymentservice_Block_Adminhtml_System_Config_Source_Descriptiondisabledtext extends Mage_Adminhtml_Block_System_Config_Form_Field{

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $element->setValue(Mage::helper('bnlpositivity_paymentservice')->__('Allow your customers to pay comfortably by credit cards'));
        $element->setDisabled('disabled');

        return parent::_getElementHtml($element);
    }
}