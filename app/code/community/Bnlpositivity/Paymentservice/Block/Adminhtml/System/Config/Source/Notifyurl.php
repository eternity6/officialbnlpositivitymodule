<?php

class Bnlpositivity_Paymentservice_Block_Adminhtml_System_Config_Source_Notifyurl extends Mage_Adminhtml_Block_System_Config_Form_Field{

  protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
  {
    return sprintf('%sbnlpositivity_paymentservice/init/notify/', Mage::getBaseUrl());
  }
}