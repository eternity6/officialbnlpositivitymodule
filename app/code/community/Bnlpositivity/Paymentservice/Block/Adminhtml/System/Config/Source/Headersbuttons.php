<?php

class Bnlpositivity_Paymentservice_Block_Adminhtml_System_Config_Source_Headersbuttons extends Mage_Adminhtml_Block_System_Config_Form_Field{

  protected function _prepareLayout()
  {
    parent::_prepareLayout();
    if (!$this->getTemplate()) {
      $this->setTemplate('bnlpositivity/system/config/source/headersbuttons.phtml');
    }
    return $this;
  }

  protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
  {
    return $this->_toHtml();
  }
}