<?php

class Bnlpositivity_Paymentservice_Block_Adminhtml_System_Config_Source_Titledisabledtext extends Mage_Adminhtml_Block_System_Config_Form_Field{

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $element->setValue(Mage::helper('bnlpositivity_paymentservice')->__('BNLPOSitivity Payment Gateway'));
        $element->setDisabled('disabled');

        return parent::_getElementHtml($element);
    }
}