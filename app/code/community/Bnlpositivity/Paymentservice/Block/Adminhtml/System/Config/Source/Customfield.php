<?php
//https://alanstorm.com/magento_system_configuration_in_depth_tutorial/
class Bnlpositivity_Paymentservice_Block_Adminhtml_System_Config_Source_Customfield extends Mage_Adminhtml_Block_System_Config_Form_Field{

  protected function _prepareLayout()
  {
    parent::_prepareLayout();
    if (!$this->getTemplate()) {
      $this->setTemplate('bnlpositivity/system/config/source/customfield.phtml');
    }
    return $this;
  }

  protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
  {
    return $this->_toHtml();
  }
}