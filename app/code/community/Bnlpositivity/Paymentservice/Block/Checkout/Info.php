<?php
/**
 * Bnlpositivity Paymentservice payment info block
 * Uses default templates
 */
class Bnlpositivity_Paymentservice_Block_Checkout_Info extends Mage_Payment_Block_Info
{

  /**
   * Prepare credit card related payment info
   *
   * @param Varien_Object|array $transport
   * @return Varien_Object
   */
  protected function _prepareSpecificInformation($transport = null)
  {
    if (null !== $this->_paymentSpecificInformation) {
      return $this->_paymentSpecificInformation;
    }

    $info = $this->getInfo();
    $helper = Mage::helper('bnlpositivity_paymentservice');
    $payment_instruments      = $helper->getPayGateway()->getPaymentInstruments();
    $payment_instrument_data  = $info->getAdditionalInformation('bnlpositivity_paymentservice_payment_instrument');
    if($payment_instrument_data === 'cc')
      $payment_instrument_label = $helper->__('Credits cards');
    else
      $payment_instrument_label = isset($payment_instruments[$payment_instrument_data]) ? $payment_instruments[$payment_instrument_data] : null;

    $transport = parent::_prepareSpecificInformation($transport);

    if($payment_instrument_label){
      $transport->addData(array(
        $helper->__('Payment instrument') => $payment_instrument_label
      ));
    }

    return $transport;
  }
}
