<?php
/**
 * Bnlpositivity payment "form"
 */
class Bnlpositivity_Paymentservice_Block_Checkout_Form extends Mage_Payment_Block_Form
{

  protected function _construct()
  {

    $mark = Mage::getConfig()->getBlockClassName('core/template');
    $mark = new $mark;
    $mark
      ->setTemplate('bnlpositivity/checkout/mark.phtml')
      ->setMarkSrc($this->getSkinUrl('bnlpositivity/images/bnlpositivity.png', array('_secure'=>true)));

    $this
      ->setTemplate('bnlpositivity/checkout/form.phtml')
      ->setMethodLabelAfterHtml($mark->toHtml());
    parent::_construct();
  }

  /**
   * Render block HTML
   *
   * @return string
   */
  protected function _toHtml()
  {
    Mage::dispatchEvent('payment_form_block_to_html_before', array(
      'block'     => $this
    ));
    return parent::_toHtml();
  }

  /**
   * Payment method code getter
   * @return string
   */
  public function getMethodCode()
  {
    return Bnlpositivity_Paymentservice_Model_Payment::CODE;
  }
}
