
var $jBNLPOSitivity = jQuery.noConflict();

$jBNLPOSitivity(document).ready(function() {
  $jBNLPOSitivity('body').on('click', '.bnlpositivity-checkout-methods-selectable table.bnlpositivity-alternative tbody td[data-name]', function (event) {
    $jBNLPOSitivity('div[data-show]').addClass('bnlpositivity-hidden');
    $jBNLPOSitivity('.bnlpositivity-checkout-methods-selectable tbody td').removeClass('active');
    $jBNLPOSitivity('.bnlpositivity-checkout-methods-selectable tbody').removeClass('active');
    $jBNLPOSitivity(this).addClass('active');
    $jBNLPOSitivity('input[name="payment[bnlpositivity_paymentservice_payment_instrument]"]').val($jBNLPOSitivity(this).data('name'));
    $jBNLPOSitivity('div[data-show*="' + $jBNLPOSitivity(this).data('name') + '"]').removeClass('bnlpositivity-hidden');
  });
  $jBNLPOSitivity('body').on('click', '.bnlpositivity-checkout-methods-selectable table.bnlpositivity-cc tbody', function (event) {
    $jBNLPOSitivity('div[data-show]').addClass('bnlpositivity-hidden');
    $jBNLPOSitivity('.bnlpositivity-checkout-methods-selectable tbody td').removeClass('active');
    $jBNLPOSitivity('.bnlpositivity-checkout-methods-selectable tbody').removeClass('active');
    $jBNLPOSitivity(this).addClass('active');
    $jBNLPOSitivity('input[name="payment[bnlpositivity_paymentservice_payment_instrument]"]').val($jBNLPOSitivity(this).data('name'));
    $jBNLPOSitivity('div[data-show*="' + $jBNLPOSitivity(this).data('name') + '"]').removeClass('bnlpositivity-hidden');
  });
});